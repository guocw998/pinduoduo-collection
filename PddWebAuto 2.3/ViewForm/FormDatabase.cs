using System;
using System.Windows.Forms;
using PddWebAuto.PddSearch.DbModel;
using PddWebAuto.Properties;

namespace PddWebAuto.ViewForm
{
    public partial class FormDatabase : Form
    {
        public FormDatabase()
        {
            InitializeComponent();
        }

        private void FormDatabase_Load(object sender, EventArgs e)
        {
            Icon = Resources.a9gw7_5tcr9_001;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime start = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd") + " 00:00:01"),
                    end = DateTime.Parse(dateTimePicker1.Value.ToString("yyyy-MM-dd") + " 23:59:59");
                DbHelper.DelDateMallBlack(start, end);
                MessageBox.Show(@"指定日期记录已删除!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}