using Net.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace PddWebAuto.Model
{
    [ObfuscationAttribute(Exclude = true, ApplyToMembers = true)]
    public class GroupItemModel
    {
        private static Random rng = new Random();
        [JsonProperty("Title")]
        public string Title { set; get; }

        [JsonProperty("colorDepth")]
        public string colorDepth { set; get; }

        [JsonProperty("hardwareConcurrency")]
        public string hardwareConcurrency { set; get; }

        [JsonProperty("pixelDepth")]
        public string pixelDepth { set; get; } = "24";

        [JsonProperty("deviceMemory")]
        public string deviceMemory { set; get; }

        [JsonProperty("windowSize")]
        public string windowSize { set; get; }

        [JsonProperty("screenSize")]
        public string screenSize { set; get; }

        [JsonProperty("fingerprintRandom")]
        public string fingerprintRandom { set; get; }

        [JsonProperty("gpuRenderer")]
        public string gpuRenderer { set; get; }
        [JsonProperty("userAgent")]
        public string userAgent { set; get; }

        [JsonProperty("macAddress")]
        public string macAddress { set; get; } = GetRandomMacAddress();
        [JsonProperty("languages")]
        public string languages { set; get; } = "zh-CN,zh";

        [JsonProperty("timezone")]
        public string timezone { set; get; } = "Asia/Shanghai";
        [JsonProperty("computerName")]
        public string computerName { set; get; } = GetRandomComputerName();
        [JsonProperty("userName")]
        public string userName { set; get; } = Net.Http.HttpHelper.RanMixingString(4);


        [JsonProperty("speechVoices")]
        public string speechVoices { set; get; } = GetRandomSpeechVoices();
        [JsonProperty("plugins")]
        public string plugins { set; get; } = GetRandomPlugins();

        [JsonProperty("position")]
        public string position { set; get; } = GetRandomPosition();

        [JsonProperty("maoTouYingArgs")]
        public string maoTouYingArgs { set; get; }

        /// <summary>
        /// 最新日志
        /// </summary>
        [JsonProperty("LastLog")]
        public string LastLog { set; get; }



        /// <summary>
        /// 放缓存路径名称 跟项目文件夹名一致用来方便删除!!是名称不是路径
        /// </summary>
        [JsonProperty("ChromeData")]
        public string ChromeData { set; get; }

        [JsonProperty("Cookies")]
        public string Cookies { set; get; }

        [JsonProperty("Enable")]
        public bool Enable { set; get; }
        /// <summary>
        /// 备注
        /// </summary>
        [JsonProperty("Des")]
        public string Des { set; get; }
        /// <summary>
        /// 最后使用代理IP
        /// </summary>
        [JsonProperty("ProxyIP")]
        public string ProxyIP { set; get; }
        /// <summary>
        /// 创建时间
        /// </summary>

        [JsonProperty("CreateTime")]
        public DateTime CreateTime { set; get; }

        public static string GetRandomMacAddress()
        {
            var random = new Random();
            var buffer = new byte[6];
            random.NextBytes(buffer);
            var result = String.Concat(buffer.Select(x => string.Format("{0}:", x.ToString("X2"))).ToArray());
            return result.TrimEnd(':');
        }


        public static string GetRandomComputerName()
        {
            return (Net.Http.HttpHelper.RanMixingString(6, DisableNumbers: true) + "-" + Net.Http.HttpHelper.RanMixingString(6)).ToUpper();
        }

        public static string GetRandomSpeechVoices() {

         List<string> speechVoicesPath= File.ReadAllText(Path.Combine(Application.StartupPath, "speechVoices.txt")).Split(';').ToList();
            Shuffle(speechVoicesPath);

            List<string> RanList = new List<string>();
          int X=  rng.Next(1, speechVoicesPath.Count - 1);
            for (int i = 0; i < X; i++)
            {
                RanList.Add(speechVoicesPath[i]);
            }

            return String.Join(";", RanList);
        }
        public static string GetRandomPlugins() {
        
            string plugins = "";
            int X = rng.Next(1,6);
            for (int i = 0; i < X; i++)
            {
                string plugName = HttpHelper.RanMixingString(rng.Next(3, 5), DisableNumbers: true);
                plugins += "plugName=" + plugName + "&plugDesc=" + HttpHelper.RanMixingString(rng.Next(3, 10), DisableNumbers: true) + "&plugPath=" + plugName + ".dll&mimeType=application/" + plugName.ToLower() + "&mimeExt=&mimeDesc=&,";
            }
            return plugins;
        }

        public static string GetRandomPosition()
        {
            return rng.Next(10, 800).ToString()+","+ rng.Next(10, 800).ToString();
        }
        public static void Shuffle<T>(IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

    }
}