using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace PddWebAuto.Model
{
  public  class GroupModel
    {
        [JsonProperty("Title")]
        public string Title { set; get; }
        [JsonProperty("ProxyUrl")]
        public string ProxyUrl { set; get; }

        [JsonProperty("UseProxy")]
        public bool UseProxy { set; get; }
        
        
        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public ChromeDrive ChromeDrive { get; set; }
    }
}
