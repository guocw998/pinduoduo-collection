using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.Model
{
    public class ReturnsGoodsInfoModel
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string order_sn;
        /// <summary>
        /// 仅退款   否则退货退款
        /// </summary>
        public bool IsRefunds;
        /// <summary>
        /// 理由
        /// </summary>
        public string Reason;
        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone;
        /// <summary>
        /// 说明
        /// </summary>
        public string Directions;

    }
}
