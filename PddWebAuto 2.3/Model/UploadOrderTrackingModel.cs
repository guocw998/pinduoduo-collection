using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.Model
{
    public class UploadOrderTrackingModel
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string order_sn;
        /// <summary>
        /// 快递单号
        /// </summary>
        public string tracking;
        /// <summary>
        /// 退货留言
        /// </summary>
        public string Directions;
        /// <summary>
        /// 运费
        /// </summary>
        public string Freight;
        /// <summary>
        /// 售后ID
        /// </summary>
        public string salesId;
    }
}
