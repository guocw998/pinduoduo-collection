using PddWebAuto.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace PddWebAuto
{
    internal class AppClass
    {
        private static string _AppPath_, _GroupPath_, _ChromeData_, _PddSearchPath_, _PddSearchDataPath_;

        /// <summary>
        /// 根配置文件夹
        /// </summary>
        public static string AppPath => _AppPath_;

        /// <summary>
        /// 组存放文件夹
        /// </summary>
        public static string GroupPath => _GroupPath_;

        /// <summary>
        /// 浏览器缓存文件夹
        /// </summary>
        public static string ChromeDataPath => _ChromeData_;

        /// <summary>
        /// 拼多多采集商品根目录
        /// </summary>
        public static string PddSearchPath => _PddSearchPath_;
        /// <summary>
        /// 拼多多采集商品导出数据目录
        /// </summary>
        public static string PddSearchDataPath => _PddSearchDataPath_;

        public static void initialize(string StartPath)
        {
            _AppPath_ = Path.Combine(StartPath, "AppData");
            _GroupPath_ = Path.Combine(_AppPath_, "Group");
            _ChromeData_ = Path.Combine(_AppPath_, "ChromeData");
            _PddSearchPath_ = Path.Combine(StartPath, "采集商品");
            _PddSearchDataPath_ = Path.Combine(_PddSearchPath_, "导出商品");
            Directory.CreateDirectory(_AppPath_);
            Directory.CreateDirectory(_GroupPath_);
            Directory.CreateDirectory(_ChromeData_);
            Directory.CreateDirectory(_PddSearchPath_);
            Directory.CreateDirectory(_PddSearchDataPath_);
        }

        /// <summary>
        /// 获取组完整路径
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static string GetGroupSubPath(string Name)
        {
            return Path.Combine(GroupPath, Name);
        }

        /// <summary>
        /// 获取对应组项目的详细文件路径
        /// </summary>
        /// <param name="GroupName"></param>
        /// <param name="ItemFileName"></param>
        /// <returns></returns>
        public static string GetGroupSubItemPath(string GroupName, string ItemFileName)
        {
            return Path.Combine(GetGroupSubPath(GroupName), ItemFileName + JsonFileUtility.DataSuffix);
        }

        /// <summary>
        /// 取浏览器指定缓存文件夹路径
        /// </summary>
        /// <param name="ChromeData"></param>
        /// <returns></returns>
        public static string GetChromeDataPath(string ChromeData)
        {
            return Path.Combine(GetGroupSubPath(ChromeDataPath), ChromeData);
        }
        /// <summary>
        /// 创建组
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static bool CreateGroupSub(GroupModel m)
        {
            string P = GetGroupSubPath(m.Title);
            if (!Directory.Exists(P))
            {
                try
                {
                    Directory.CreateDirectory(P);
                    Thread.Sleep(50);
                    Save_GroupModel(m, m.Title);
                    return true;
                }
                catch (Exception)
                {
                }
            }

            return false;
        }

        /// <summary>
        /// 获取全部组名称
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllGroup()
        {
            List<string> Names = new List<string>();
            foreach (var item in Directory.GetDirectories(GroupPath))
            {
                Names.Add(Path.GetFileNameWithoutExtension(item));
            }
            return Names;
        }

        /// <summary>
        /// 获取组下边所有项目
        /// </summary>
        /// <param name="GroupName"></param>
        /// <returns></returns>
        public static Dictionary<string, GroupItemModel> GetGroupAllItem(string GroupName)
        {
            JsonFileUtility JsonFile = new JsonFileUtility(GetGroupSubPath(GroupName));
            var m = JsonFile.GetFiles<GroupItemModel>();
            m.Remove("GroupData");
            return m;
        }

        /// <summary>
        /// 保存组浏览器配置 成功返回浏览器配置文件名称
        /// </summary>
        /// <param name="m"></param>
        /// <param name="GroupName"></param>
        /// <param name="ItemFileName"></param>
        /// <returns></returns>
        public static string Save_GroupItem(GroupItemModel m, string GroupName, string ItemFileName)
        {
            if (Directory.Exists(GetGroupSubPath(GroupName)))
            {
                JsonFileUtility JsonFile = new JsonFileUtility(GetGroupSubPath(GroupName));
                return JsonFile.SetFile<GroupItemModel>(m, ItemFileName);
            }
            
            return null;
        }
        /// <summary>
        /// 保存组数据模型数据
        /// </summary>
        /// <param name="m"></param>
        /// <param name="GroupName"></param>
        /// <returns></returns>
        public static string Save_GroupModel(GroupModel m, string GroupName)
        {
            if (Directory.Exists(GetGroupSubPath(GroupName)))
            {
                JsonFileUtility JsonFile = new JsonFileUtility(GetGroupSubPath(GroupName));
                return JsonFile.SetFile<GroupModel>(m, "GroupData");
            }
            return null;
        }
        /// <summary>
        /// 获取组模型数据
        /// </summary>
        /// <param name="GroupName"></param>
        /// <returns></returns>
        public static GroupModel Get_GroupModel(string GroupName)
        {
            if (File.Exists(GetGroupSubItemPath(GroupName, "GroupData")))
            {
                JsonFileUtility JsonFile = new JsonFileUtility(GetGroupSubPath(GroupName));
                return JsonFile.GetFile<GroupModel>("GroupData");
            }
            return new GroupModel() { Title= GroupName };
        }
        /// <summary>
        /// 组重命名
        /// </summary>
        /// <param name="GroupName"></param>
        /// <param name="NewGroupName"></param>
        /// <returns></returns>
        public static bool GroupRenaming(string GroupName, string NewGroupName)
        {
            string GP = GetGroupSubPath(GroupName);
            string NewGP = GetGroupSubPath(NewGroupName);
            if (Directory.Exists(NewGP))
            {
                return false;
            }
            try
            {
                Directory.Move(GP, NewGP);
                Thread.Sleep(30);
                return true;
            }
            catch (Exception)
            {

             
            }
            return false;
        }


        /// <summary>
        /// 移动项目到其他组
        /// </summary>
        /// <param name="GroupName">旧组名</param>
        /// <param name="ItemFileName">项目文件名</param>
        /// <param name="NewGroupName">待移动新组名</param>
        /// <returns></returns>
        public static bool Move_GroupItem(string GroupName, string NewGroupName, string ItemFileName)
        {
            string _Path = GetGroupSubItemPath(GroupName, ItemFileName);
            string NewPath = GetGroupSubItemPath(NewGroupName, ItemFileName);

            try
            {
                File.Move(_Path, NewPath);
                return true;
            }
            catch (Exception)
            {
            }
            return false;
        }

        /// <summary>
        /// 删除浏览器缓存
        /// </summary>
        /// <param name="ChromeData"></param>
        /// <returns></returns>
        public static bool Del_ChromeData(string ChromeData)
        {
            string _Path = GetChromeDataPath(ChromeData);
            if (Directory.Exists(_Path))
            {
                try
                {
                    Directory.Delete(_Path, true);
                    return true;
                }
                catch (Exception)
                {
                }
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// 删除组下的项目
        /// </summary>
        /// <param name="GroupName"></param>
        /// <param name="ItemFileName"></param>
        /// <returns></returns>
        public static bool Del_GroupItem(string GroupName, string ItemFileName)
        {
            if (Del_ChromeData(ItemFileName))
            {
                string _Path = GetGroupSubItemPath(GroupName, ItemFileName);
                if (File.Exists(_Path))
                {
                    try
                    {
                        File.Delete(_Path);
                        return true;
                    }
                    catch (Exception)
                    {
                    }
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 删除组
        /// </summary>
        /// <param name="GroupName"></param>
        /// <returns></returns>
        public static bool Del_Group(string GroupName)
        {
          

            string _Path = GetGroupSubPath(GroupName);
            JsonFileUtility JsonFile = new JsonFileUtility(_Path);
            foreach (var FileName in JsonFile.GetFileNames())
            {
                if (!Del_GroupItem(GroupName, FileName))
                {
                    return false;
                }
            }
            if (Directory.Exists(_Path))
            {
                try
                {
                    Directory.Delete(_Path,true);
                    return true;
                }
                catch (Exception)
                {
                }
                return false;
            }
            else
            {
                return true;
            }
        }



        /// <summary>
        /// 检查名称是否合法
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static bool CheckName(string Name)
        {
            return Name.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;
        }
    }
}