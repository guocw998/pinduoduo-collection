using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;

namespace PddWebAuto
{
    static class Program
    {
        public static int _Run_ = 0;
        public static CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();
        public static  Logger Logger { get; set; }
        
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            LogManager.Configuration.Variables["appType"] = $"Pdd";
            Logger=LogManager.GetLogger("Logger");
            AppClass.initialize(Application.StartupPath);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Frm());
        }
    }
}