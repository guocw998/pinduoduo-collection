using System.Threading.Tasks;

namespace PddWebAuto.SmsHandler
{
    public interface ISMSAPI
    {
        Task<string> WaitSms(int Second, params string[] Args);
        Task<string> GetPhone(int ReNum = 5);

        /// <summary>
        ///     获取错误消息或者其他消息
        /// </summary>
        /// <returns></returns>
        string GetMsg();

        bool IsUse();
    }
}