using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Data.SQLite;
using System.Data.SQLite.EF6;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PddWebAuto.PddSearch.DbModel
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public partial class DbModel : DbContext
    {
        public DbModel() : base(new SQLiteConnection("Data Source=\"" + Path.Combine(AppClass.PddSearchPath, "PddData.db") + "\""),true)

        {
            DbConfiguration.SetConfiguration(new SQLiteConfiguration());
        }

        public virtual DbSet<MallBlackTable> MallBlackTable { get; set; }
 
    }

    public class SQLiteConfiguration : DbConfiguration
    {
        public SQLiteConfiguration()
        {
            SetProviderFactory("System.Data.SQLite", SQLiteFactory.Instance);
            SetProviderFactory("System.Data.SQLite.EF6", SQLiteProviderFactory.Instance);
            SetProviderServices("System.Data.SQLite", (DbProviderServices)SQLiteProviderFactory.Instance.GetService(typeof(DbProviderServices)));
        }
    }
}
