using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.PddSearch.DbModel
{
    class DbHelper
    {
        /// <summary>
        /// 锁对象
        /// </summary>
        public static readonly object lockObj = new object();

        /// <summary>
        /// 判断账户是否存在
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool MallIsExist(long id)
        {
            using (var d = new DbModel())
            {
                var data = d.MallBlackTable.FirstOrDefault(a => a.mall_id == id);

                return data != null && data.mall_id == id;
            }
        }

        /// <summary>
        /// 添加店铺黑名单记录
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        public static bool MallAdd(long id, string Name)
        {
            try
            {
                using (var d = new DbModel())
                {
                    var mails =  d.MallBlackTable.Where(x => x.mall_id == id || x.mall_name == Name).ToList();
                    if (mails.Count > 0) return false;
                    d.MallBlackTable.Add(new MallBlackTable()
                        { dateTime = DateTime.Now, mall_id = id, mall_name = Name });
                    return d.SaveChanges() > 0;
                }
            }
            catch (Exception)
            {
                // ignored
                return false;
            }
        }

        public static void DelDateMallBlack(DateTime Start, DateTime End)
        {
            using (var d = new DbModel())
            {
                var SQL = "delete from MallBlackTable where MallBlackTable.dateTime > '" +
                          Start.ToString("yyyy-MM-dd HH:mm:ss") + "' AND  MallBlackTable.dateTime < '" +
                          End.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                d.Database.ExecuteSqlCommand(SQL);
                d.SaveChanges();
            }
        }
    }
}