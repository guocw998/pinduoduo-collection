namespace PddWebAuto.PddSearch.DbModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Reflection;

    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    [Table("MallBlackTable")]
    public class MallBlackTable
    {
        [StringLength(100)]
        public string mall_name { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long mall_id { get; set; }
        
        [Required]
        public DateTime dateTime { get; set; }
    }
}
