using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PddWebAuto.Model;
using PddWebAuto.PddModel;

namespace PddWebAuto.PddSearch
{
    internal class PddApi
    {
        private static readonly Random Dom = new Random();
        private readonly GroupModel _group;


        //public SearchModelInfo SearchInitialize(string Key)
        //{
        //    Cookies = "PDDAccessToken=" + UserToken;
        //    var Res = Get("http://mobile.yangkeduo.com/");
        //    if (Res.IsSuccessStatusCode)
        //    {
        //        await TaskHelper.Delay(1000 * Dom.Next(1, 3));
        //        try
        //        {
        //            // Cookies = Res.Cookie;
        //            //Cookies = "api_uid=" + HttpHelper.CookiesVal(Cookies, "api_uid") + "; pdd_vds=" + HttpHelper.CookiesVal(Cookies, "pdd_vds") + "; PDDAccessToken=" + UserToken;
        //            Res = Get("http://app.yangkeduo.com/search_result.html?search_key=" + Uri.EscapeDataString(Key) + "&search_met_track=manual&search_type=goods&source=index&options=3&refer_page_name=search_result&refer_page_el_sn=99884&&refer_page_id=10015_" + HttpHelper.TimeStampMilliseconds() + "_9g78uyxibc&refer_page_sn=10015");
        //            if (Res.IsSuccessStatusCode)
        //            {
        //                string rawData = Regex.Match(Res.Html, "window.rawData=([\\s\\S]*?\\});")?.Groups[1]?.Value;
        //                var Json = JObject.Parse(rawData);
        //                string flip = (string)Json.SelectToken("stores.store.data.ssrListData.flip") ?? "";
        //                //!string.IsNullOrEmpty(flip)
        //                if (true)
        //                {
        //                    string page = (string)Json.SelectToken("stores.store.data.ssrListData.page") ?? "1";
        //                    SearchModelInfo Info = new SearchModelInfo();
        //                    Info.page = int.Parse(page);
        //                    Info.flip = flip;
        //                    Info.q = Key;
        //                    Info.list_id = (string)Json.SelectToken("stores.store.data.listID") ?? "";
        //                    Info.track_data = (string)Json.SelectToken("stores.store.data.urlInfo.urlApiParams.track_data") ?? "";
        //                    Info.is_new_query = (int)Json.SelectToken("stores.store.data.urlInfo.is_new_query");

        //                    Info.pdduid = HttpHelper.CookiesVal(Cookies, "pdd_user_id");
        //                    Info.refer_page_id_token = "9g78uyxibc";

        //                    return Info;
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //        }
        //    }
        //    return null;
        //}

        #region anti_content

        // private string AntiContentCode =
        //     "dmFyIGM9WyJsMXlRVzVSY1NXPT0iLCJ6dkpjUXZaZE5hPT0iLCAiVzRoZFBTb2JXUHZ5IiwgIm5XbGRLQ29Jdkc9PSIsCiAgICAiQ2VUeWgzSz0iLAogICAgInBhL2NWZXhjTEc9PSIsCiAgICAiY21rMFc2SmRVU29LIiwKICAgICJBd1N4VzVaY0hxPT0iLAogICAgImpJcGNLZmRjT1c9PSIsCiAgICAiVzVyNVdRWHBXNzQ9IiwKICAgICJuOGsxbW1vSFc0Rz0iLAogICAgInhlNEpXN0ZjTVc9PSIsCiAgICAiaG1vbHc4a1ZpVz09IiwKICAgICJnZnV0VzZoY1NHPT0iLAogICAgImhmbGNWU2t6clc9PSIsCiAgICAialpwY1JOL2NScT09IiwKICAgICJXN3RkVjhrRiIsCiAgICAiaWcwVVc3VmNMVz09IiwKICAgICJiMDNkR0NrQldQMD0iLAogICAgIm5ZRmNQVz09IiwKICAgICJXNHVlVzZTdFdQMD0iLAogICAgIlc0QmROOG9nV1I5RCIsCiAgICAicWU4OXFDbzMiLAogICAgIlc2OGRnbWtTV1I0PSIsCiAgICAiQWUwRnNtb0QiLAogICAgInBTb1ZFQ2tvakc9PSIsCiAgICAiVzZhcGxTb0JmRz09IiwKICAgICJtcS9kUjhvbXlhPT0iLAogICAgImFta01pQ29qVzQwPSIsCiAgICAieE41R1dQVmNKYT09IiwKICAgICJXNjdkSm1rNFdRamkiLAogICAgImZ4UmNWQ2s3eUc9PSIsCiAgICAiZlNrTG9Tb0xXN2E9IiwKICAgICJhOG9DV1BKZFA4a3QiLAogICAgImU4bzBXUnhkSThrdiIsCiAgICAiQ2hPM1c2TmNNYT09IiwKICAgICJhd1ZkUG1rR1dPMD0iLAogICAgIm5DazBXNnBkTUNvZCIsCiAgICAiVzR4ZFA4a09XTzVKIiwKICAgICJsU293eFNrMGZXPT0iLAogICAgImpzL2NQd1ZjVFc9PSIsCiAgICAiV09KZFJtbzlhbWt0IiwKICAgICJuc1JjVUxkY1Vta0giLAogICAgImdDa0lXNEZkTG1vRiIsCiAgICAiRG1vdlc3ZXJ6Rz09IiwKICAgICJjU29GRDhrZmVxPT0iLAogICAgIldSVmNIOG91VzdhQyIsCiAgICAiV1B2Q1c2eGNLU2tyIiwKICAgICJXNHFSVzRhcldRVz0iLAogICAgIldQcGNQZ2pmRlc9PSIsCiAgICAiZlNvaHJDazBjRz09IiwKICAgICJXNEZkTW1vdFdSdmUiLAogICAgIlc3YkpXUTFDVzZDPSIsCiAgICAiVzVLNmJDb29XNmk9IiwKICAgICJkU2tqVzd0ZFJTb0IiLAogICAgImp0eGNVZlJjUnE9PSIsCiAgICAiQUxqMldRUmRRRz09IiwKICAgICJXNUJkU1NrcVdPS0giLAogICAgImxLMDdXUER5IiwKICAgICJmOG9TVzZWY05ycT0iLAogICAgImVTb3dDU2tvYWE9PSIsCiAgICAiZDhvR1c3QmNQSU89IiwKICAgICJtMEZjUkNrRXRxPT0iLAogICAgInF2M2NPdUpkVnE9PSIsCiAgICAiaU1HNVc1QmNWYT09IiwKICAgICJXNzNkVkNvNldQRDIiLAogICAgIlc2VmRLbWtPV084dyIsCiAgICAienVlSUI4b3oiLAogICAgIkNta2hXUDBuVzVXPSIsCiAgICAiVzdsZExta1NXT2ZoIiwKICAgICJXNUZkSXFkY0pTa08iLAogICAgImFDa0JwbW9QeUc9PSIsCiAgICAibDI3ZElDa2dXUks9IiwKICAgICJzMDVBV1I3Y1RhPT0iLAogICAgImJ0dGNOaGRjVVc9PSIsCiAgICAiZ0psZEs4a0hGVz09IiwKICAgICJXNVNzbzhvWFc0aT0iLAogICAgIkZnQzBXN2hjTm1vcXdhPT0iLAogICAgInhta1BoZERsIiwKICAgICJlMTRrV1J6USIsCiAgICAiQk5GY1Z4cGRQcT09IiwKICAgICJ6MXZhZEswPSIsCiAgICAiVzd5T2lDazJXUTA9IiwKICAgICJxTGI3bGcwPSIsCiAgICAidDhvNkJ3aGNPcT09IiwKICAgICJnbWs2bFlEOVdQZGNIU29RcUc9PSIsCiAgICAib3FsZEdta2lDcT09IiwKICAgICJybW8rdUtsY1NXPT0iLAogICAgImRTb0lXT1ZkUThrQyIsCiAgICAiaVhTVXNOdT0iLAogICAgIlc1aXBXNFM3V1JTPSIsCiAgICAiV1B0Y1R2T0N0Rz09IiwKICAgICJBM0NjQW1vUyIsCiAgICAibENvdFc2bGNNYmE9IiwKICAgICJpdUd6V1BMeiIsCiAgICAiV1FWZFBtb0tlU2tSIiwKICAgICJXNHlkb0NrcVdRND0iLAogICAgImpDb2JXNDdjTlhDPSIsCiAgICAiVzR0ZEpDa05XT0NKIiwKICAgICJoQ28vVzdaY1NKOD0iLAogICAgIkJOdVpXNk5jTUc9PSIsCiAgICAiYjhrRlc2aGROOG9OIiwKICAgICJXNFNwb0NrWFdRSz0iLAogICAgImNYZGRPbWtERmE9PSIsCiAgICAiVzYzZEhTb3lXUWZ0IiwKICAgICJXNmxkU21rMFdSajQiLAogICAgIkEyYkhXT3RjSGVlTXlxPT0iLAogICAgImYzVmNTU2sveEc9PSIsCiAgICAicWcxdSIsCiAgICAiZnR5aXZnYT0iLAogICAgIkRDa2hwc2ZlIiwKICAgICJXUjNjS21vM29NV0V3OGtLIiwKICAgICJ5ZXYzIiwKICAgICJXNHhkTUtTZWpibT0iLAogICAgIlc3OTdXT0w3VzRtPSIsCiAgICAiVzZ4ZE9Da0tXUVh3IiwKICAgICJnY0NVeWUwPSIsCiAgICAiVzdXWGttb21iOGtUIiwKICAgICJjOGtJZXNEMCIsCiAgICAiV09UcEVXPT0iLAogICAgInlTbzNFOG9WV1B5PSIsCiAgICAiaU55aFc1bGNOTE5jRzhrWVdRdT0iLAogICAgIlc3SmRNU2tmV1JuRCIsCiAgICAiRmZpalc1dGNIVz09IiwKICAgICJ4Q29rVzU0WnpxPT0iLAogICAgIlc3N2RVc2k9IiwKICAgICJXNUZkSGZhNmVxPT0iLAogICAgIkUxRmNRdlZkU0c9PSIsCiAgICAiZVovZE5DbzRBRz09IiwKICAgICJDZ1BtV1FaZEthPT0iLAogICAgIkE4b0xFQ29KV1BTPSIsCiAgICAib0NvU1c3VmNUSkM9IiwKICAgICJtQ29BRGE9PSIsCiAgICAiVzdEWHVTb3VEcT09IiwKICAgICJpYzNkUUNvOHVhPT0iLAogICAgInJOM2NJYT09IiwKICAgICJXNi9kSjhrUFdSR1EiLAogICAgIlc0eGRMWWxjUG1rYyIsCiAgICAiRjNKY1B2WmRMYT09IiwKICAgICJ4Q2s4aUhuNCIsCiAgICAicWcxNSIsCiAgICAiVzUvZEw4b09XUHI0IiwKICAgICJoVzQxQzNDPSIsCiAgICAic1NvWnp3eGNQVz09IiwKICAgICJ5d2RjVXZOZFVXPT0iLAogICAgInQwVHpXUXBkSUc9PSIsCiAgICAibHY3ZEpTb0lqcT09IiwKICAgICJXNVR6eHE9PSIsCiAgICAiVzZEbldRSz0iLAogICAgIlc1bUdhQ2tGV1JDPSIsCiAgICAiVzZMbVdPNStXNkM9IiwKICAgICJXUjdkUW1vSmE4aysiLAogICAgImVta0ZXNGRkT21vYiIsCiAgICAiaW1rOGltb05FYT09IiwKICAgICJXNFpkUDhrYVdQdmMiLAogICAgIkY4azRXTzQwVzRlPSIsCiAgICAiY1NvSEU4azljRz09IiwKICAgICJqdzRUVzVkY1NXPT0iLAogICAgInd1SmNPS1JkVGE9PSIsCiAgICAic3dOY1F4L2RHRz09IiwKICAgICJhQ2tTaUNvTUVxPT0iLAogICAgIlc2cGRTOG93V1FUSCIsCiAgICAiV1JGZFFtb25qbWtUIiwKICAgICJjS0JkR0NrcFdPbT0iLAogICAgIm9Db1dXNFZjUElhPSIsCiAgICAiV1FkZFNTb1VqbWtzIiwKICAgICJjOGtkVzVKZE04b0UiLAogICAgIlc3YjBBR3ZsIiwKICAgICJzQ2s0V095bFc2MD0iLAogICAgIm5YTmRTbWtYdlc9PSIsCiAgICAiVzY3ZFJTa2pXT3FqIiwKICAgICJXNDRFY0NvaFc2Tz0iLAogICAgIlc2ZGRQbWtwV1JITiIsCiAgICAiVzd0ZFZJVmNPU2tSIiwKICAgICJxZzNkVkc9PSIsCiAgICAiVzdPZmNtb2ZkYT09IiwKICAgICJXUkRtVzVWY0xxPT0iLAogICAgIkNTb1JXNFc0QXE9PSIsCiAgICAibW1vMFdQM2RWbWtqIiwKICAgICJpOG9tVzZaY1BkOD0iLAogICAgIkNTa2FXUXl2VzRtPSIsCiAgICAiQUNrTVdRQ0xXNHE9IiwKICAgICJXNXBkT0NrMFdSdjMiLAogICAgIlc3eURXNDRTV1A4PSIsCiAgICAiV1JQOFc1ZGNObWtkIiwKICAgICJ5bWtOYUlENSIsCiAgICAiY2ZlVFdSVDYiLAogICAgIlc2V2RibWttV08wPSIsCiAgICAiZVNvM1dRbGRWQ2tVIiwKICAgICJXNWZsd1pybCIsCiAgICAiV1BWY1RlNHRXUXU9IiwKICAgICJEdUNQdW1vayIsCiAgICAiaExwY0tDa3NxWGU9IiwKICAgICJnM2hkVUNrb1dSdT0iLAogICAgInNMMHNXNkpjUFc9PSIsCiAgICAibGY3ZEw4b09wRz09IiwKICAgICJ3OGs0V1BXSlc3dT0iLAogICAgImkwOG1XNWRjVVc9PSIsCiAgICAia2IvZFU4a2xzVz09IiwKICAgICJXT2hjTVNvVyIsCiAgICAiVzVMbmZHPT0iLAogICAgIkY4a0pXUW14VzZtPSIsCiAgICAiVzVsZFUwQ0RjYT09IiwKICAgICJlS1JkS21rb1dQRz0iLAogICAgInRtb3VXNjA9IiwKICAgICJnU2tyVzdKZFZTb3IiLAogICAgIldQTmNQOG9jIiwKICAgICJEaExBbUxXPSIsCiAgICAic1NvMEVmZGNRcT09IiwKICAgICJXNnlnVzY4OVdRcT0iLAogICAgIlc2Q1BpbWtJV1FhPSIsCiAgICAiV1JKZExtb3luU2tZIiwKICAgICJXNWlpbUNrRFdSYT0iLAogICAgIm9NaGROOGtQV1JIViIsCiAgICAiZU5xUVdRSG4iLAogICAgImJta2FrU29IVzR1PSIsCiAgICAiVzRQeEVidk4iLAogICAgIldRaGNReFNXeVc9PSIsCiAgICAieENvS0VXPT0iLAogICAgImd1QmNJU2syeUc9PSIsCiAgICAibnZpUlc0QmNTcT09IiwKICAgICJtM3RjVm1rWENKOVlXUXlYZDhrdVdRZkpXNzFmV1BtbldSaitXUjF0VzZXYlc0UERkQ2tya0xiRHM4b3pXUjRneVNveXYyMHJXTzNkSkpwZEloOURXUGhjR0NvY3RLRmNOOGtUVzZuSHZiTFJrZzlNZUtoZEhDb1AiLAogICAgIlc3aVpmbW9sVzRxPSIsCiAgICAicDFKZEdTazRXUFc9IiwKICAgICJuczNjVHVoY01TazZ1OGtqIiwKICAgICJxOGttaHI1cCIsCiAgICAibFdDeHRLVz0iLAogICAgInBtaytoU29ZRkc9PSIsCiAgICAiYmRGZEtta0l3YT09IiwKICAgICJXUi9jTVNvTCIsCiAgICAiY3NDeSIsCiAgICAiVzdCZEtDa21XUGZPIiwKICAgICJ0Q2tlV1B5WFc3MD0iLAogICAgInNta1ZXUks9IiwKICAgICJkTkZkUVNva2lxPT0iLAogICAgIlc1T3lvQ29MVzVPPSIsCiAgICAiVzRSY0laMHhXNWhkUENrYVdQZGRPMGFvRThvQ3dYVmNTZ2JWdFdicVc2dT0iLAogICAgImlLTmRLOGtoV1JhPSIsCiAgICAiV1F0ZFFDb21tU2tnIiwKICAgICJXNmRkVThrMVdROTQiLAogICAgIkFTb1hBTVJjSEc9PSIsCiAgICAiZ01oZEtDb0JuYT09IiwKICAgICJlQ2s1bVNvRVc2SzJ2OG9jdGJLPSIsCiAgICAicG1vK0Zta2ZlYT09IiwKICAgICJmM3k4V1BMMEV4ND0iLAogICAgIm9Ta21tOG9jenE9PSIsCiAgICAiVzdsZEs4b1dXUm5yVzZXdHFNRzBXNy9jTXhiVSIsCiAgICAiVzd1d2Rtb2ZiRz09IiwKICAgICJBOG9xeXVkY1BHPT0iLAogICAgInM4b0h0M0ZjVHE9PSIsCiAgICAiYThva0JDa0FkcT09IiwKICAgICJXN212ZzNPSSIsCiAgICAiRThrTFdSMGRXN2k9IiwKICAgICJXNzhxaEtTRiIsCiAgICAiVzZYTVdSSHNXNks9IiwKICAgICJoQ295elNrN2ZhPT0iLAogICAgIldRTmNLU29IcDFTPSIsCiAgICAib0NrYWlDb2NXNmk9IiwKICAgICJiU29FVzVaY1ZYcT0iLAogICAgIlc1cGRWQ2tIV1JqMyIsCiAgICAiZWVoZE5Tb0doRz09IiwKICAgICJXNFZkVG1raFdSTz0iLAogICAgIlc3M2RNdGU9IiwKICAgICJicUJjSmVsY1RHPT0iLAogICAgIldPcGNLTFhXQmE9PSIsCiAgICAiVzd1UmEwT0tud3BkUm1vcSIsCiAgICAiV08zY0tTb0hXN0M0IiwKICAgICJXUFJjT0NvZmwwaT0iLAogICAgIkJ4dk9XUGhjU2E9PSIsCiAgICAiaHdLMFc3dGNKcT09IiwKICAgICJCTU9qVzVsY0dxPT0iLAogICAgImNtb3VXT05kVW1rOCIsCiAgICAiRThrOVdReWpXN05kTmE9PSIsCiAgICAiV1JOY1FTb0ZpMFM9IiwKICAgICJ6TFRIV1BwY1VXPT0iLAogICAgIldSUGpXN0JjTENrQiIsCiAgICAiQkxSY0xNZGRMVz09IiwKICAgICJzOGt6V09paVc1bT0iLAogICAgIlc0MG1XNHVxV1A4PSIsCiAgICAiaTEzY01DazdFYT09IiwKICAgICJXUUJjTE11cFdPdT0iLAogICAgIng4bzJ4bW9EIiwKICAgICJoQ2tCY0NvTHZXPT0iLAogICAgIkZta0VXUlNoVzVxPSIsCiAgICAiVzU4aWttbytXN0s9IiwKICAgICJXNEtlaG1rU1dPRz0iLAogICAgIldRWmNMQ29kIiwKICAgICJXUXRjSGdYSENhPT0iLAogICAgIlc0bGRSYnBjU21rWSIsCiAgICAicjhvS1c1dWtyMGUrZ1c9PSIsCiAgICAiZFNralc0RmRMQ29ZIiwKICAgICJjR2E2RWU0PSIsCiAgICAiVzY5cHltb1Z1Vz09IiwKICAgICJXUVJjU0NvN2kwaT0iLAogICAgIlc1UmRJQ29XV1FQYVc3MG9kZTQ9IiwKICAgICJjZmlOV09EcyIsCiAgICAiVzdyeldQci9XNHU9IiwKICAgICJ5U2t1ZWN6KyIsCiAgICAiVzRxc1c3MFdXT3E9IiwKICAgICJXNVZkUzhrbVdQWHoiLAogICAgIlc0NGpXN1c9IiwKICAgICJweFJjR1c9PSIsCiAgICAieWU1aG5ncGRVYT09IiwKICAgICJXUlJjUWZUMHZhPT0iLAogICAgIldReGNJbW91VzdDWSIsCiAgICAicUxSY0pLZGRUYT09IiwKICAgICJwOG82cThrVWRXPT0iLAogICAgIlc0bmxXUkx2VzZXPSIsCiAgICAicDNoZFE4a3pXT2U9IiwKICAgICJXNGVGZUNvalc1Vz0iLAogICAgIlc0M2ROQ29NV1JHPSIsCiAgICAibk5DcVc3bGNRVz09IiwKICAgICJGQ29xdzNkY1VxPT0iLAogICAgIlc0QmRHU2tLV1E4KyIsCiAgICAicm1vOHExL2NLVz09IiwKICAgICJEMGFzc21vdiIsCiAgICAiZjBlUVdPRFUiLAogICAgIm5KWFZmQ281VzZWY1ZJbmlXUEtLY0NrcFdPMGZXNjNkTkk0ZldQemlpU2tXRW1vd1dPMTJBS3FOV1F2UHlDa01tYjhhQ0Nvblc3ZGRRQ2tteHMzY0czeGRKdXVNVzdGZEpDb3FXUW5kc21rOVdRenpXNW1nV1AvY1VIbXgiLAogICAgInBDb1J5bWthYkNvcXRhPT0iLAogICAgImkyeGRJbWsrIiwKICAgICJvd0ZkVlNra1dPbT0iLAogICAgIldQTmNLMUgrQ2E9PSIsCiAgICAiVzRGZEtKeGNJQ2tQIiwKICAgICJXNGhkTlNrdVdPND0iLAogICAgIlc3R29sOG9BVzZPPSIsCiAgICAiVzYxUldSck9XNHk9IiwKICAgICJXN3FBbjhrc1dRSz0iLAogICAgIldQVmNSdldOV09HPSIsCiAgICAieG1veXJ3RmNRVz09IiwKICAgICJXT3o3VzRoY1JTa0IiLF0KdmFyIHUgPSBmdW5jdGlvbiB0KG4sIHIpIHsKICAgIHZhciBlID0gY1tuIC09IDBdOwogICAgdm9pZCAwID09PSB0LmRrZlZ4SyAmJiAodC5qUlJ4Q1MgPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgZm9yICh2YXIgciA9IFtdLCBlID0gMCwgbyA9IHZvaWQgMCwgYSA9ICIiLCBpID0gIiIsIGMgPSAwLCB1ID0gKHQgPSBmdW5jdGlvbih0KSB7CiAgICAgICAgICAgIGZvciAodmFyIG4sIHIsIGUgPSBTdHJpbmcodCkucmVwbGFjZSgvPSskLywgIiIpLCBvID0gIiIsIGEgPSAwLCBpID0gMDsgciA9IGUuY2hhckF0KGkrKyk7IH5yICYmIChuID0gYSAlIDQgPyA2NCAqIG4gKyByIDogciwKICAgICAgICAgICAgYSsrICUgNCkgPyBvICs9IFN0cmluZy5mcm9tQ2hhckNvZGUoMjU1ICYgbiA+PiAoLTIgKiBhICYgNikpIDogMCkKICAgICAgICAgICAgICAgIHIgPSAiYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXpBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWjAxMjM0NTY3ODkrLz0iLmluZGV4T2Yocik7CiAgICAgICAgICAgIHJldHVybiBvCiAgICAgICAgfSh0KSkubGVuZ3RoOyBjIDwgdTsgYysrKQogICAgICAgICAgICBpICs9ICIlIiArICgiMDAiICsgdC5jaGFyQ29kZUF0KGMpLnRvU3RyaW5nKDE2KSkuc2xpY2UoLTIpOwogICAgICAgIHQgPSBkZWNvZGVVUklDb21wb25lbnQoaSk7CiAgICAgICAgdmFyIGQgPSB2b2lkIDA7CiAgICAgICAgZm9yIChkID0gMDsgZCA8IDI1NjsgZCsrKQogICAgICAgICAgICByW2RdID0gZDsKICAgICAgICBmb3IgKGQgPSAwOyBkIDwgMjU2OyBkKyspCiAgICAgICAgICAgIGUgPSAoZSArIHJbZF0gKyBuLmNoYXJDb2RlQXQoZCAlIG4ubGVuZ3RoKSkgJSAyNTYsCiAgICAgICAgICAgICAgICBvID0gcltkXSwKICAgICAgICAgICAgICAgIHJbZF0gPSByW2VdLAogICAgICAgICAgICAgICAgcltlXSA9IG87CiAgICAgICAgZCA9IDAsCiAgICAgICAgICAgIGUgPSAwOwogICAgICAgIGZvciAodmFyIFcgPSAwOyBXIDwgdC5sZW5ndGg7IFcrKykKICAgICAgICAgICAgZSA9IChlICsgcltkID0gKGQgKyAxKSAlIDI1Nl0pICUgMjU2LAogICAgICAgICAgICAgICAgbyA9IHJbZF0sCiAgICAgICAgICAgICAgICByW2RdID0gcltlXSwKICAgICAgICAgICAgICAgIHJbZV0gPSBvLAogICAgICAgICAgICAgICAgYSArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKHQuY2hhckNvZGVBdChXKSBeIHJbKHJbZF0gKyByW2VdKSAlIDI1Nl0pOwogICAgICAgIHJldHVybiBhCiAgICB9CiAgICAgICAgLAogICAgICAgIHQudkRSQmloID0ge30sCiAgICAgICAgdC5ka2ZWeEsgPSAhMCk7CiAgICB2YXIgbyA9IHQudkRSQmloW25dOwogICAgcmV0dXJuIHZvaWQgMCA9PT0gbyA/ICh2b2lkIDAgPT09IHQuRU9FTGJaICYmICh0LkVPRUxiWiA9ICEwKSwKICAgICAgICBlID0gdC5qUlJ4Q1MoZSwgciksCiAgICAgICAgdC52RFJCaWhbbl0gPSBlKSA6IGUgPSBvLAogICAgICAgIGUKfQogICAgLCBkID0gdSgiMHgxMDUiLCAiVDVkWSIpCiAgICAsIFcgPSB1KCIweDE0MyIsICJ0blJWIikKICAgICwgZiA9IHUoIjB4ZjMiLCAicjZjeCIpCiAgICAsIHggPSB1KCIweDEzZSIsICJyNmN4IikKICAgICwgcyA9IHUoIjB4ZmMiLCAiWUQ5SiIpCiAgICAsIGggPSB1KCIweGNlIiwgIjBKSXEiKQogICAgLCBsID0gdSgiMHhmNCIsICJIYVhbIikKICAgICwgdiA9IHUoIjB4NmEiLCAiYk5kIyIpCiAgICAsIGsgPSB1KCIweDEyMSIsICIwXUpKIikKICAgICwgbSA9IHUoIjB4MTI2IiwgIncoRHEiKQogICAgLCBfID0gdSgiMHhmMiIsICJpRiVWIikKICAgICwgcCA9IHUoIjB4YzAiLCAiODZJJCIpCiAgICAsIGIgPSB1KCIweDJhIiwgIkRAR1IiKQogICAgLCBDID0gdSgiMHgxMTkiLCAiKGspRyIpCiAgICAsIGcgPSB1KCIweGRkIiwgIjg2SSQiKVtmXSgiIikKICAgICwgUyA9IHsKICAgICIrIjogIi0iLAogICAgIi8iOiAiXyIsCiAgICAiPSI6ICIiCn07CmZ1bmN0aW9uIHkodCkgewogICAgcmV0dXJuIHRbeF0oL1srXC89XS9nLCAoZnVuY3Rpb24odCkgewogICAgICAgICAgICByZXR1cm4gU1t0XQogICAgICAgIH0KICAgICkpCn0KZnVuY3Rpb24gcHBwKHQsIG4pIHsKICAgIHZhciByID0gdQogICAgICAgICwgZSA9IHt9OwogICAgZVtyKCIweDEzMyIsICJ2cXBrIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgIHJldHVybiB0ID09PSBuCiAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHhkMCIsICJCdWlwIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCA9PT0gbgogICAgICAgIH0KICAgICAgICAsCiAgICAgICAgZVtyKCIweDQ4IiwgIjFZUlAiKV0gPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgICAgIHJldHVybiB0ID49IG4KICAgICAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHgxM2MiLCAiSGFYWyIpXSA9IGZ1bmN0aW9uKHQsIG4pIHsKICAgICAgICAgICAgcmV0dXJuIHQgKyBuCiAgICAgICAgfQogICAgOwogICAgdmFyIG8gPSBlOwogICAgcmV0dXJuIG9bcigiMHhhIiwgImlGJVYiKV0odCwgNjQpID8gNjQgOiBvW3IoIjB4YzIiLCAidjddayIpXSh0LCA2MykgPyBuIDogb1tyKCIweDQ2IiwgIk5aTSYiKV0odCwgbikgPyBvW3IoIjB4MTI5IiwgIlpkNVoiKV0odCwgMSkgOiB0Cn0KCmZ1bmN0aW9uIGVuY29kZSh0LCBuKSB7CiAgICB2YXIgciA9IHUKICAgICAgICAsIGUgPSB7fTsKICAgIGVbcigiMHgzIiwgIjBJXUMiKV0gPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgcmV0dXJuIHQgPCBuCiAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHgxMzIiLCAicjZjeCIpXSA9IHIoIjB4MTNkIiwgIlt3eWoiKSwKICAgICAgICBlW3IoIjB4MTBlIiwgInY3XWsiKV0gPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgICAgIHJldHVybiB0IDwgbgogICAgICAgIH0KICAgICAgICAsCiAgICAgICAgZVtyKCIweDExYiIsICJZRDlKIildID0gcigiMHg3MSIsICJadV1EIiksCiAgICAgICAgZVtyKCIweDRiIiwgInV6YWIiKV0gPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgICAgIHJldHVybiB0ICE9PSBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4N2IiLCAidjddayIpXSA9IHIoIjB4NTUiLCAiaiZlciIpLAogICAgICAgIGVbcigiMHgxMzciLCAiSG9mXSIpXSA9IHIoIjB4MTQiLCAidURyZCIpLAogICAgICAgIGVbcigiMHhjIiwgInI2Y3giKV0gPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgICAgIHJldHVybiB0ICogbgogICAgICAgIH0KICAgICAgICAsCiAgICAgICAgZVtyKCIweGRiIiwgIjg2SSQiKV0gPSByKCIweGQ1IiwgIjFZUlAiKSwKICAgICAgICBlW3IoIjB4NDUiLCAiNVcwUiIpXSA9IHIoIjB4ZWMiLCAiV21XUCIpLAogICAgICAgIGVbcigiMHhhOSIsICJ1emFiIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCB8IG4KICAgICAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHhjYiIsICIxWVJQIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCA8PCBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4MWEiLCAiRHRuXSIpXSA9IGZ1bmN0aW9uKHQsIG4pIHsKICAgICAgICAgICAgcmV0dXJuIHQgJiBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4NjkiLCAiVDVkWSIpXSA9IGZ1bmN0aW9uKHQsIG4pIHsKICAgICAgICAgICAgcmV0dXJuIHQgLSBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4NWMiLCAiW3d5aiIpXSA9IGZ1bmN0aW9uKHQsIG4pIHsKICAgICAgICAgICAgcmV0dXJuIHQgPj4gbgogICAgICAgIH0KICAgICAgICAsCiAgICAgICAgZVtyKCIweDEzOCIsICJOYWEmIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCAtIG4KICAgICAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHg0MCIsICJIb2ZdIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCAmIG4KICAgICAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHg1MiIsICJGVkVSIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCA+PiBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4MTAwIiwgInBSYnciKV0gPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgICAgIHJldHVybiB0IC0gbgogICAgICAgIH0KICAgICAgICAsCiAgICAgICAgZVtyKCIweDY4IiwgIncoRHEiKV0gPSBmdW5jdGlvbih0LCBuKSB7CiAgICAgICAgICAgIHJldHVybiB0KG4pCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4NTQiLCAiQnVpcCIpXSA9IGZ1bmN0aW9uKHQsIG4sIHIpIHsKICAgICAgICAgICAgcmV0dXJuIHBwcChuLCByKQogICAgICAgIH0KICAgICAgICAsCiAgICAgICAgZVtyKCIweDgwIiwgIjBJXUMiKV0gPSBmdW5jdGlvbih0LCBuLCByKSB7CiAgICAgICAgICAgIHJldHVybiBwcHAobiwgcikKICAgICAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHgxYyIsICJpRiVWIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCB8IG4KICAgICAgICB9CiAgICAgICAgLAogICAgICAgIGVbcigiMHhhMSIsICJ3KERxIildID0gZnVuY3Rpb24odCwgbikgewogICAgICAgICAgICByZXR1cm4gdCA8PCBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4OWIiLCAiWUQ5SiIpXSA9IGZ1bmN0aW9uKHQsIG4pIHsKICAgICAgICAgICAgcmV0dXJuIHQgKyBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4NzIiLCAidnFwayIpXSA9IGZ1bmN0aW9uKHQsIG4pIHsKICAgICAgICAgICAgcmV0dXJuIHQgKyBuCiAgICAgICAgfQogICAgICAgICwKICAgICAgICBlW3IoIjB4NmQiLCAid1dVNiIpXSA9IGZ1bmN0aW9uKHQsIG4pIHsKICAgICAgICAgICAgcmV0dXJuIHQgKyBuCiAgICAgICAgfQogICAgO2ZvciAodmFyIGEsIGksIGMsIGQsIFcgPSBlLCBmID0gewogICAgICAgICJfYsOHIjogdCA9IHQsCiAgICAgICAgX2JLOiAwLAogICAgICAgIF9iZjogZnVuY3Rpb24oKSB7CiAgICAgICAgICAgIHZhciBuID0gcjsKICAgICAgICAgICAgcmV0dXJuIHRbX10oZltuKCIweDhjIiwgImJOZCMiKV0rKykKICAgICAgICB9CiAgICB9LCBzID0gewogICAgICAgICJfw6oiOiBbXSwKICAgICAgICAiX2LDjCI6IC0xLAogICAgICAgICJfw6EiOiBmdW5jdGlvbih0KSB7CiAgICAgICAgICAgIHZhciBuID0gcjsKICAgICAgICAgICAgc1tuKCIweDdkIiwgIlQ1ZFkiKV0rKywKICAgICAgICAgICAgICAgIHNbIl/DqiJdW3NbbigiMHhjOCIsICJ2cXBrIildXSA9IHQKICAgICAgICB9LAogICAgICAgICJfYsOdIjogZnVuY3Rpb24oKSB7CiAgICAgICAgICAgIHZhciB0ID0gcjsKICAgICAgICAgICAgcmV0dXJuIF9iw51bdCgiMHgxMWUiLCAiV21XUCIpXS0tLAogICAgICAgICAgICBXW3QoIjB4OGQiLCAidyhEcSIpXShfYsOdW3QoIjB4Y2MiLCAiTmFhJiIpXSwgMCkgJiYgKF9iw51bdCgiMHgxMDYiLCAidG5SViIpXSA9IDApLAogICAgICAgICAgICAgICAgX2LDnVsiX8OqIl1bX2LDnVt0KCIweGFlIiwgImJOZCMiKV1dCiAgICAgICAgfQogICAgfSwgaCA9ICIiLCBsID0gV1tyKCIweDciLCAidjddayIpXSwgdiA9IDA7IFdbcigiMHgxNDIiLCAiTlpNJiIpXSh2LCBsW3BdKTsgdisrKQogICAgICAgIHNbIl/DoSJdKGxbV1tyKCIweGM1IiwgIkhvZl0iKV1dKHYpKTsKICAgIHNbIl/DoSJdKCI9Iik7CiAgICB2YXIgayA9IFdbcigiMHgxMTgiLCAiV21XUCIpXSh2b2lkIDAgPT09IG4gPyAidW5kZWZpbmVkIiA6IHVuZGVmaW5lZCwgV1tyKCIweDZiIiwgIjg2SSQiKV0pID8gTWF0aFtXW3IoIjB4YjUiLCAiWUQ5SiIpXV0oV1tyKCIweDhmIiwgIkJ1aXAiKV0oTWF0aFtXW3IoIjB4YmQiLCAidE0hbiIpXV0oKSwgNjQpKSA6IC0xOwogICAgZm9yICh2ID0gMDsgV1tyKCIweDExIiwgIkhvZl0iKV0odiwgdFtwXSk7IHYgPSBmW3IoIjB4NzAiLCAiJk5HXiIpXSkKICAgICAgICBmb3IgKHZhciBtID0gV1tyKCIweDMyIiwgInI2Y3giKV1bcigiMHgzNyIsICJEQEdSIildKCJ8IiksIGIgPSAwOyA7ICkgewogICAgICAgICAgICBzd2l0Y2ggKG1bYisrXSkgewogICAgICAgICAgICAgICAgY2FzZSAiMCI6CiAgICAgICAgICAgICAgICAgICAgaSA9IFdbcigiMHhkZSIsICJFWCY5IildKFdbcigiMHgxMmYiLCAiVmRCWCIpXShXW3IoIjB4MTIwIiwgIk5aTSYiKV0oc1siX8OqIl1bV1tyKCIweDVkIiwgIjRqOUAiKV0oc1tyKCIweDdkIiwgIlQ1ZFkiKV0sIDIpXSwgMyksIDQpLCBXW3IoIjB4MTM5IiwgInRuUlYiKV0oc1siX8OqIl1bV1tyKCIweDQ3IiwgIlBvcSYiKV0oc1tyKCIweDg3IiwgInY3XWsiKV0sIDEpXSwgNCkpOwogICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlOwogICAgICAgICAgICAgICAgY2FzZSAiMSI6CiAgICAgICAgICAgICAgICAgICAgZCA9IFdbcigiMHg4OSIsICJOWk0mIildKHNbIl/DqiJdW3NbcigiMHg4NCIsICI0ajlAIildXSwgNjMpOwogICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlOwogICAgICAgICAgICAgICAgY2FzZSAiMiI6CiAgICAgICAgICAgICAgICAgICAgc1siX8OhIl0oZltyKCIweDEwIiwgIjVXMFIiKV0oKSk7CiAgICAgICAgICAgICAgICAgICAgY29udGludWU7CiAgICAgICAgICAgICAgICBjYXNlICIzIjoKICAgICAgICAgICAgICAgICAgICBhID0gV1tyKCIweDUyIiwgIkZWRVIiKV0oc1siX8OqIl1bV1tyKCIweGM5IiwgIllEOUoiKV0oc1tyKCIweGU5IiwgIlpkNVoiKV0sIDIpXSwgMik7CiAgICAgICAgICAgICAgICAgICAgY29udGludWU7CiAgICAgICAgICAgICAgICBjYXNlICI0IjoKICAgICAgICAgICAgICAgICAgICBXW3IoIjB4M2MiLCAiVWNiVyIpXShpc05hTiwgc1siX8OqIl1bV1tyKCIweDY0IiwgInY3XWsiKV0oc1tyKCIweDEyZCIsICJIYVhbIildLCAxKV0pID8gYyA9IGQgPSA2NCA6IFdbcigiMHg3MyIsICJUNWRZIildKGlzTmFOLCBzWyJfw6oiXVtzW3IoIjB4NzciLCAieUA1dSIpXV0pICYmIChkID0gNjQpOwogICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlOwogICAgICAgICAgICAgICAgY2FzZSAiNSI6CiAgICAgICAgICAgICAgICAgICAgc1siX8OhIl0oZltyKCIweGM3IiwgInBSYnciKV0oKSk7CiAgICAgICAgICAgICAgICAgICAgY29udGludWU7CiAgICAgICAgICAgICAgICBjYXNlICI2IjoKICAgICAgICAgICAgICAgICAgICBXW3IoIjB4OGEiLCAiJld2aiIpXSh2b2lkIDAgPT09IG4gPyAidW5kZWZpbmVkIiA6IHVuZGVmaW5lZCwgV1tyKCIweDYwIiwgIkZWRVIiKV0pICYmIChhID0gV1tyKCIweGVlIiwgInJpYiUiKV0obiwgYSwgayksCiAgICAgICAgICAgICAgICAgICAgICAgIGkgPSBXW3IoIjB4MTQ5IiwgInlANXUiKV0obiwgaSwgayksCiAgICAgICAgICAgICAgICAgICAgICAgIGMgPSBXW3IoIjB4OSIsICJ2cXBrIildKG4sIGMsIGspLAogICAgICAgICAgICAgICAgICAgICAgICBkID0gV1tyKCIweGZmIiwgInI2Y3giKV0obiwgZCwgaykpOwogICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlOwogICAgICAgICAgICAgICAgY2FzZSAiNyI6CiAgICAgICAgICAgICAgICAgICAgYyA9IFdbcigiMHgxNDQiLCAiRVgmOSIpXShXW3IoIjB4YTciLCAidE0hbiIpXShXW3IoIjB4NTgiLCAieFklbyIpXShzWyJfw6oiXVtXW3IoIjB4YjkiLCAiWmQ1WiIpXShzW3IoIjB4ZTYiLCAiREBHUiIpXSwgMSldLCAxNSksIDIpLCBXW3IoIjB4ZmEiLCAiVWNiVyIpXShzWyJfw6oiXVtzW3IoIjB4N2QiLCAiVDVkWSIpXV0sIDYpKTsKICAgICAgICAgICAgICAgICAgICBjb250aW51ZTsKICAgICAgICAgICAgICAgIGNhc2UgIjgiOgogICAgICAgICAgICAgICAgICAgIGggPSBXW3IoIjB4MTM0IiwgIjFZUlAiKV0oV1tyKCIweDEwYSIsICIwSklxIildKFdbcigiMHgxMTIiLCAiYk5kIyIpXShXW3IoIjB4M2IiLCAiNGo5QCIpXShoLCBzWyJfw6oiXVthXSksIHNbIl/DqiJdW2ldKSwgc1siX8OqIl1bY10pLCBzWyJfw6oiXVtkXSk7CiAgICAgICAgICAgICAgICAgICAgY29udGludWU7CiAgICAgICAgICAgICAgICBjYXNlICI5IjoKICAgICAgICAgICAgICAgICAgICBzWyJfw6EiXShmW3IoIjB4NmMiLCAiYk5kIyIpXSgpKTsKICAgICAgICAgICAgICAgICAgICBjb250aW51ZTsKICAgICAgICAgICAgICAgIGNhc2UgIjEwIjoKICAgICAgICAgICAgICAgICAgICBzW3IoIjB4ODciLCAidjddayIpXSAtPSAzOwogICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlCiAgICAgICAgICAgIH0KICAgICAgICAgICAgYnJlYWsKICAgICAgICB9CiAgICByZXR1cm4gV1tyKCIweDFlIiwgIlQ1ZFkiKV0oaFt4XSgvPS9nLCAiIiksIGxba10gfHwgIiIpCn0KdmFyIGE9KCd4XHg5Q1x4OTVceDkxPW/Dk0BceDAwXHg4Nlx4OTNceDAwXHgwM8O8XHgwMVx4MTBceDEyw5JNIVx4OTXDgHZ8PsOnw5xceDEyQVx4MTJceDlBwqRyw6xceDA2UsKww5tceDBFw5bDlcK+OFx4MTfDm3dceDhFw61UcVx4ODQgXHgxMlx4MTIwMsOCw4bDisOUwq1iY0DDvFx4MTFWflx4MDRfQlx4ODhceDExw7bDp3lceDg3w6fCvVDCrVRqw7cvX1x4MEY+wr3CvFlceDAxw4fDtW8hw55UZ0VceDkxw6bDm8KyXHg5Q1x4ODhceDEzXHgxNlPCqSQ8XHg4Q2jCsFx4MTRceDkyL1x4MTLDmVx4OEZJXHg5RcKzKcOzScOBXHgwNFx4OTdmRVx4MTLDn8ONw6hceDk0Zl5KQsOqcVx4OTLDkHZceDlBw7PDmDvCpcOZXHgxRsKsw75ceDE3w4FceDgydlNceDgxXHg4NsOiNXVceDg4wqBceDgxwrHCplx4MUFIw7FceDkwXHg4OMOow4pceDgwwqfDocKyXsO5w6hceDlDfcK5w7fCtsO2w7zDosOwXHg4Nlx4ODYhw5RceDE1wqPCpcK2wrTCpsKqwqrCt1x4N0ZKXHg5QcKiIVx4MDQxw4bDo03Dl33Dlj3Dnlx4OTBceDkyXHg5NXnDrWrCuXpRwrXDhFx4OUHDhTFceDkxXHg5McKkXHg4MMOGXHg4OMOxw6VqXHgwN3R4XHg5MFx0XHgxNlx4MDBdUnZceDgwTVfDi1x4MUMgw5BdwrI4XHg5MMKtXHg4N1x4MURceDg0w60tw5BJw5NceDk4OsO0w4RkXHg4NVx4OENgS1x4ODI6aFx4OTjDg1x4MDNrdFx2w4QswqJgQMO9SGxceDgxw54sXHgxM1x0XHg5NVx4OERceDFGw6tceDkywqZDLCFccljCvypceDgxXHRceDk5XHg5Mlx4OEPDvVx4OTY3XHI3w51dwrhceDhCw6TDkF1ceDhDXHgxN256IFzDg8OrXHLDnMO9wqdceDk4OcKqXHg3RsKoKXbCucK/XHg5RU5xw77Dqlx4OUbDicOXw4rDv8O+w7LDplx4OEFceDhFXHg4Q2ZceDEzQsKsw6rDr8Kuw7UiXHg5NcKrTj/DhMKuw4HCu2HDt8ORw4BceDhGw6ZceDlEwrDDnX5ceDdGw6nCvMO6w6TDs8O5w5nDl1x4MEZ3QjJceDFGTcO2XHg5Q2QyXHgxRcOuwqXCtmnDt1x2w4fCtsK4wrN7NMK2XHgwNmlyNFx4OUFzw7PDgVx4ODTDtVx4OTPCucOtMndgXHg4RcOXIi3Dg8OEJFx4OEbCuSXCvlx4MDPCusK1w4LCrCcrICdceDEwJDEsJykKdmFyIGI9cHBwCmZ1bmN0aW9uIGppZWd1bygpewogICAgcmV0dXJuICcwYXAnK2VuY29kZShhLGIpCn0=";

        #endregion anti_content

        private string Cookies = "";
        private string IPPorxy;
        public string UserAgent = "";

        public PddApi(GroupModel group)
        {
            _group = group;
            RanUa();
        }


        private void RanUa()
        {
            try
            {
                var UAs = File.ReadAllText(Path.Combine(AppClass.PddSearchPath, "UserAgent.txt"))
                    .Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                UserAgent = UAs[Dom.Next(0, UAs.Length)].Trim();
            }
            catch (Exception)
            {
                MessageBox.Show("UserAgent 切换异常");
            }
        }

        //public JObject SearchApi(SearchModelInfo info)
        //{
        //    // string anti = HttpHelper.RunJavascriptCode(Encoding.UTF8.GetString(Convert.FromBase64String(AntiContentCode)) + "\r\njieguo();");
        //    string anti = Form1.TokenWeb.GetAntiContent(Cookies);
        //    var Res = Get("http://app.yangkeduo.com/proxy/api/search?pdduid=" + info.pdduid + "&item_ver=lzqq&source=index&search_met=manual&track_data=" + info.track_data + "&list_id=" + info.list_id + "&sort=" + info.sort + "&filter=" + info.filter + "&q=" + Uri.EscapeDataString(info.q) + "&page=" + info.page.ToString() + "&flip=" + info.flip + "&is_new_query=" + info.is_new_query.ToString() + "&size=50&anti_content=" + anti);
        //    if (Res.IsSuccessStatusCode)
        //    {
        //        var Json = JObject.Parse(Res.Html);
        //        var flip = Json.SelectToken("flip");
        //        if (flip != null && !string.IsNullOrEmpty((string)flip))
        //        {
        //            info.flip = (string)flip;
        //        }
        //        return Json;
        //    }
        //    return null;
        //}

        /// <summary>
        ///     获取商品信息
        /// </summary>
        /// <param name="m"></param>
        public bool GetGoodsInfo(GoodsInfoModel m)
        {
            try
            {
                RanUa();
                Cookies = "";
                if (_group.UseProxy)
                {
                    IPPorxy = _group.ProxyUrl;
                }
                var Res = Get("http://app.yangkeduo.com");
                if (Res.IsSuccessStatusCode)
                {
                    Cookies = "webp=1; api_uid=" + HttpHelper.CookiesVal(Cookies, "api_uid") + "; pdd_vds=" +
                              HttpHelper.CookiesVal(Cookies, "pdd_vds");
                    m.Link = "http://mobile.yangkeduo.com/goods.html?goods_id=" + m.Goods_id;
                    Res = Get("http://app.yangkeduo.com/goods.html?goods_id=" + m.Goods_id);
                    //+ "&refer_page_name=index&refer_page_id=10002_" + HttpHelper.TimeStampMilliseconds() + "_5a7p0wqn5r&refer_page_sn=10002"
                    if (Res.IsSuccessStatusCode)
                    {
                        var rawData = Regex.Match(Res.Html, "window.rawData=([\\s\\S]*?\\});")?.Groups[1]?.Value;
                        Debug.WriteLine(rawData);
                        if (rawData.Contains("{\"needLogin\":true}"))
                        {
                            return false;
                        }
                        var Json = JObject.Parse(rawData);
                        m.Evaluation = (int)Json.SelectToken("store.initDataObj.oakData.review.reviewNum");
                        var servicePromise = Json.SelectToken("store.initDataObj.oakData.servicePromise") as JArray;
                        if (servicePromise != null)
                        {
                            foreach (var item in servicePromise)
                            {
                                m.Goods_tags += (string)item["type"] + "/";
                            }

                            m.Goods_tags = m.Goods_tags.TrimEnd('/');
                        }
                        m.Mall_name = (string)Json.SelectToken("store.initDataObj.mall.mallName");
                        m.Sales = (string)Json.SelectToken("store.initDataObj.goods.sideSalesTip");
                        var combine_group_desc =
                            (string)Json.SelectToken(
                                "store.initDataObj.goods.neighborGroup.neighbor_data.combine_group.combine_group_desc") ??
                            "";
                        var Match = Regex.Match(combine_group_desc, "(\\d+)");
                        m.MergeNum = 0;
                        if (Match.Success)
                        {
                            m.MergeNum = int.Parse(Match.Groups[1].Value);
                        }
                        var skus = Json.SelectToken("store.initDataObj.goods.skus") as JArray;
                        var GoodsInfos = new List<GoodsSkuModel>();
                        if (skus != null)
                        {
                            foreach (var item in skus)
                            {
                                var skuId = (string)item["skuId"];
                                var quantity = (int)item["quantity"];
                                var groupPrice = (string)item["groupPrice"];
                                var Price = (string)item.SelectToken("priceDisplay.price") ??
                                            (string)item.SelectToken("groupPrice");
                                var specs = item.SelectToken("specs") as JArray;
                                var specsList = new List<string>();
                                if (specs != null)
                                {
                                    foreach (var spec in specs)
                                    {
                                        specsList.Add((string)spec["spec_value"]);
                                    }
                                }
                                GoodsInfos.Add(new GoodsSkuModel
                                    { skuId = skuId, price = Price, quantity = quantity, skus = specsList });
                            }
                        }
                        m.Skus = JsonConvert.SerializeObject(GoodsInfos);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return false;
        }

        private HttpResult Get(string Url)
        {
            //IPPorxy = "192.168.10.3:8888";
            Debug.WriteLine(Url + "   " + IPPorxy + "   " + UserAgent);
            var Item = new HttpItem();
            Item.URL = Url;
            Item.ContentType = null;
            Item.Timeout = 5 * 1000;
            Item.ReadWriteTimeout = 5 * 1000;
            Item.Accept =
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
            Item.UserAgent = UserAgent.Trim();
            if (!string.IsNullOrWhiteSpace(IPPorxy))
            {
                Item.ProxyIp = IPPorxy;
            }
            //Item.Referer = "http://app.yangkeduo.com/";
            // Item.Header["Accept-Encoding"] = "gzip, deflate";
            Item.Header["Accept-Language"] = "zh-CN,zh;q=0.9,en;q=0.8";
            Item.Header["Cache-Control"] = "no-cache";
            Item.Header["Upgrade-Insecure-Requests"] = "1";
            Item.Header["Pragma"] = "no-cache";
            //if (!string.IsNullOrEmpty(Verifyauthtoken))
            //{
            //    Item.Header["verifyauthtoken"] = Verifyauthtoken;
            //}
            if (!string.IsNullOrEmpty(Cookies))
            {
                //string PDDAccessToken = HttpHelper.CookiesVal(Cookies, "PDDAccessToken");
                //if (!string.IsNullOrEmpty(PDDAccessToken))
                //{
                //    Item.Header["accesstoken"] = PDDAccessToken;
                //}
                Item.Cookie = Cookies;
            }
            var Res = Item.Send();
            Cookies = HttpHelper.CookiesUpdate(Res.Cookie, Cookies);
            return Res;
        }
    }
}