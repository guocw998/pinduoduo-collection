using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace PddWebAuto
{
    public class JsonFileUtility
    {
        /// <summary>
        /// 目标跟目录
        /// </summary>
        private string _RootPath = null;
       public const string DataSuffix = ".dat";
        private static Mutex mutex = new Mutex(false, "PDDWEBAUTO_JSONFiles");

        public JsonFileUtility(string RootPath)
        {
            _RootPath = RootPath;
            Directory.CreateDirectory(_RootPath);

        }

        /// <summary>
        /// 获取目录执行文件模型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Dictionary<string, T> GetFiles<T>()
        {
            Dictionary<string, T> dic = new Dictionary<string, T>();
            string[] Files = Directory.GetFiles(_RootPath, "*"+ DataSuffix);
            foreach (var item in Files)
            {
                string t = File.ReadAllText(item);
                try
                {
                    var m = JsonConvert.DeserializeObject<T>(t);
                    dic.Add(Path.GetFileNameWithoutExtension(item), m);
                }
                catch (Exception)
                {
                    dic.Add(Path.GetFileNameWithoutExtension(item), default);
                }
            }
            return dic;
        }

        /// <summary>
        /// 获取指定名称单个文件模型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Name"></param>
        /// <returns></returns>
        public T GetFile<T>(string Name)
        {
            string FilePath = Path.Combine(_RootPath, Name+ DataSuffix);
            if (File.Exists(FilePath))
            {
                string t = File.ReadAllText(FilePath);
                try
                {
                    var m = JsonConvert.DeserializeObject<T>(t);
                    return m;
                }
                catch (Exception)
                {
                }
            }
            return default;
        }
        /// <summary>
        /// 获取目标所有匹配文件名不带扩展名
        /// </summary>
        /// <returns></returns>
        public List<string> GetFileNames()
        {
            List<string> Names = new List<string>();
            string[] Files = Directory.GetFiles(_RootPath, "*" + DataSuffix);
            foreach (var item in Files)
            {
                Names.Add(Path.GetFileNameWithoutExtension(item));
            }
            return Names;
        }


        /// <summary>
        /// 保存模型到指定文件 成功返回文件名称 失败返回Null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="m"></param>
        /// <param name="FileName">Null随机名称 同名将覆盖</param>
        /// <returns></returns>
        public string SetFile<T>(T m, string FileName)
        {
            try
            {
                string Json = JsonConvert.SerializeObject(m);
                if (FileName == null)
                {
                    FileName = DateTime.Now.Ticks.ToString();
                }
                string SaveN = FileName;
                string SavePath = Path.Combine(_RootPath, FileName+ DataSuffix);
                //mutex.WaitOne()
                if (true)
                {
                    try
                    {
                        File.WriteAllText(SavePath, Json);
                    }
                    finally
                    {
                        //mutex.ReleaseMutex();
                    }
                }

                return SaveN;
            }
            catch (Exception)
            {
            }
            return null;
        }
        /// <summary>
        /// 删除数据文件
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public bool Delete(string Name)
        {
            string FilePath = Path.Combine(_RootPath, Name+ DataSuffix);
            try
            {
                File.Delete(FilePath);
                return true;
            }
            catch (Exception)
            {


            }
            return false;
        }
    }
}