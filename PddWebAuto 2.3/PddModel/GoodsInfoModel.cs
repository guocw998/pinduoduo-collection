using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.PddModel
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class GoodsInfoModel
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public string Goods_id { get; set; }
        /// <summary>
        /// 商品标题
        /// </summary>
        public string Goods_name { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public string Price_info { get; set; }
        /// <summary>
        /// 店铺ID
        /// </summary>
        public long Mall_id { get; set; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        public string Mall_name { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        public string Link { get; set; }
        /// <summary>
        /// 商品标签
        /// </summary>
        public string Goods_tags { get; set; }
        /// <summary>
        /// 评价数量 商品评价
        /// </summary>
        public int Evaluation { get; set; }
        /// <summary>
        /// 销量已拼
        /// </summary>
        public string Sales { get; set; }
        /// <summary>
        /// 当前拼单数量
        /// </summary>
        public int MergeNum { get; set; }
        public string Skus { get; set; }
    }
}
