using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.PddModel
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class AfterSaleModel
    {
        /// <summary>
        /// 浏览器ID
        /// </summary>
        public string BrowserID { set; get; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderSn { set; get; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string goodsName { set; get; }
        
        /// <summary>
        /// 店铺名称
        /// </summary>
        public string MallName { set; get; }
        /// <summary>
        /// 商品ID
        /// </summary>
        public string GoodsId { set; get; }
        /// <summary>
        /// 售后ID
        /// </summary>
        public string afterSalesId { set; get; }
        /// <summary>
        /// SkuID
        /// </summary>
        public string SkuId { set; get; }
        /// <summary>
        /// Sku
        /// </summary>
        public string SkuSpec { set; get; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public string OrderStatus { set; get; }
        /// <summary>
        /// 支付价格
        /// </summary>
        public string OrderAmount { set; get; }
        // <summary>
        /// 退货地址
        /// </summary>
        public string ReturnAddress { set; get; }
        // <summary>
        /// 申请时间
        /// </summary>
        public string SqTime { set; get; }
    }
}
