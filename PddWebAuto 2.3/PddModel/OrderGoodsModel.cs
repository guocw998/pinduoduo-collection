using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.PddModel
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class OrderGoodsModel
    {
        /// <summary>
        /// 浏览器ID
        /// </summary>
        public string BrowserID { set; get; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderSn { set; get; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string goodsName { set; get; }
        /// <summary>
        /// 支付价格
        /// </summary>
        public string OrderAmount { set; get; }
        /// <summary>
        /// 店铺名称
        /// </summary>
        public string MallName { set; get; }
        /// <summary>
        /// 商品ID
        /// </summary>
        public string GoodsId { set; get; }
        /// <summary>
        /// SkuID
        /// </summary>
        public string SkuId { set; get; }
        /// <summary>
        /// Sku
        /// </summary>
        public string SkuSpec { set; get; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public string OrderStatus { set; get; }
        /// <summary>
        /// 物流费用
        /// </summary>
        public string Suffix { set; get; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public string CreateTime { set; get; }
        /// <summary>
        /// 发货时间
        /// </summary>
        public string DeliveryTime { set; get; }
        /// <summary>
        /// 快递公司
        /// </summary>
        public string CourierName { set; get; }
        /// <summary>
        /// 最新物流进程
        /// </summary>
        public string LatestProgress { set; get; }
        
        /// <summary>
        /// 签收时间
        /// </summary>
        public string SigningTime { set; get; }
        /// <summary>
        /// 最新物流状态
        /// </summary>
        public string LogisticsStatus { set; get; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string CourierNumber { set; get; }
        /// <summary>
        /// 收货人电话
        /// </summary>
        public string Mobile { set; get; }
        /// <summary>
        /// 收货人姓名
        /// </summary>
        public string ReceiveName { set; get; }
        // <summary>
        /// 省
        /// </summary>
        public string ProvinceName { set; get; }
        // <summary>
        /// 市
        /// </summary>
        public string CityName { set; get; }
        // <summary>
        /// 区
        /// </summary>
        public string DistrictName { set; get; }
        // <summary>
        /// 详细地址
        /// </summary>
        public string ShippingAddress { set; get; }


    }
}
