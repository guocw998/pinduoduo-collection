using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.PddModel
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    public class GoodsSkuModel
    {

        public string skuId { get; set; }


        public List<string> skus { get; set; } = new List<string>();

        public int quantity { get; set; }
        public string price { get; set; }
    }
}
