using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PddWebAuto.Model;

namespace PddWebAuto.PddModel
{
    public class SearchSettingModel
    {
        /// <summary>
        /// 搜索数量
        /// </summary>
        public int MaxSearchNum = 0;

        /// <summary>
        /// 反向过滤标签
        /// </summary>
        public List<string> FilterTag = new List<string>();

        public int WheelStartL = 0;
        public int WheelEndL = 100;

        public int WheelStartSleep = 1000;
        public int WheelEndSleep = 3000;

        /// <summary>
        /// 并发
        /// </summary>
        public bool BfSearch { get; set; }


        /// <summary>
        /// 1页数 3匹配数量 4日志
        /// </summary>
        public Action<int, string> SetStatusLabel;

        public GroupModel Group { get; set; }
    }
}