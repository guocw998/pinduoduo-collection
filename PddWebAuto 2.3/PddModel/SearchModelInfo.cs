using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PddWebAuto.PddModel
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    class SearchModelInfo
    {
        /// <summary>
        /// 排序 default /  _comment(好评) _sales(销量) price(价格上) _price(价格下) _brand(品牌)
        /// </summary>
        public string sort { get; set; } = "default";
        /// <summary>
        /// 搜索关键词
        /// </summary>
        public string q { get; set; }
        public string flip { get; set; }
        public int page { get; set; }
        public string list_id { get; set; }
        public string pdduid { get; set; }
        public string item_ver { get; set; } = "lzqq";
        public string source { get; set; } = "index";
        public string search_met { get; set; } = "manual";
        public string track_data { get; set; }
        public string refer_page_id_token { get; set; }
        public int is_new_query { get; set; }

        public string filter { get; set; } = "";
    }
}
