using System.Threading;
using System.Threading.Tasks;

namespace PddWebAuto
{
    public class TaskHelper
    {
        /// <summary>
        /// 延迟
        /// </summary>
        /// <param name="delay"></param>
        public static async Task Delay(int delay)
        {
            var cancel = CancellationTokenSource.CreateLinkedTokenSource(Program.CancellationTokenSource.Token);

            await Task.Delay(delay, cancel.Token);
        }
    }
}