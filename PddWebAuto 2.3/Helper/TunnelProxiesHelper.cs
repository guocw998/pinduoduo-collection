using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace PddWebAuto.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public class TunnelProxiesHelper
    {
        /// <summary>
        /// 刷新代理
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        public static async Task<bool> RefreshProxyAsync(string port)
        {
            try
            {
                var url = $"http://127.0.0.1:9988/tunnel/Refresh?port={port}";
                //请求
                var rsp = await WebHelper.HttpGetAsync(url, default, 10000);
                if (string.IsNullOrWhiteSpace(rsp)) return false;
                var data = JObject.Parse(rsp);
                if (data.ContainsKey("data"))
                {
                    return data["data"]?.Value<bool>() ?? false;
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }
    }
}